import cookieParser from 'cookie-parser';
import express from 'express';
import logger from 'morgan';

import productsRouter from './routes/products';
import authRouter from './routes/auth';
import customersRouter from './routes/customers';
import categoriesRouter from './routes/categories';

import axios from "axios";

const app = express();

axios.defaults.baseURL = process.env.PRESTASHOP_API_URL;
axios.defaults.headers.common['Authorization'] = `Basic ${process.env.PRESTASHOP_API_KEY}`;

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/products', productsRouter);
app.use('/auth', authRouter);
app.use('/customers', customersRouter);
app.use('/categories', categoriesRouter);

export default app;
