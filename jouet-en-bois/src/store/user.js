import * as ApplicationSettings from 'application-settings';

export default {
    state: {
        firstname: undefined,
        lastname: undefined,
        email: undefined,
        birthday: undefined
    },
    mutations: {
        /**
         * Update user's data
         * User data is stored in the application's local storage
         * @param state
         * @param userData user's data
         */
        setUserData (state, { userData }) {
            state.email = userData.email;
            state.firstname = userData.firstname;
            state.lastname = userData.lastname;
            state.birthday = userData.birthday;
            ApplicationSettings.setString('firstname', userData.firstname);
            ApplicationSettings.setString('lastname', userData.lastname);
            ApplicationSettings.setString('email', userData.email);
            ApplicationSettings.setString('birthday', userData.birthday);
        },
        /**
         * Update user's email
         *
         * @param state
         * @param email User's email
         */
        setEmail (state, { email }) {
            state.email = email;
            ApplicationSettings.setString('email', email);
        },
        /**
         * Update user's email
         *
         * @param state
         * @param email User's email
         */
        setFirstname (state, { firstname }) {
            state.firstname = firstname;
            ApplicationSettings.setString('firstname', firstname);
        },
        /**
         * Update user's email
         *
         * @param state
         * @param email User's email
         */
        setLastname (state, { lastname }) {
            state.lastname = lastname;
            ApplicationSettings.setString('lastname', lastname);
        },
        /**
         * Update user's email
         *
         * @param state
         * @param email User's email
         */
        setBirthday (state, { birthday }) {
            state.birthday = birthday;
            ApplicationSettings.setString('birthday', birthday);
        },
        /**
         * Disconnect a user, removing all it's data from the local storage
         *
         * @param state
         */
        disconnect (state) {
            ApplicationSettings.remove('firstname');
            ApplicationSettings.remove('lastname');
            ApplicationSettings.remove('email');
            ApplicationSettings.remove('birthday');
        }
    },
    getters: {
        firstname (state) {
            return state.firstname ?? ApplicationSettings.getString('firstname');
        },
        lastname (state) {
            return state.lastname ?? ApplicationSettings.getString('lastname');
        },
        email (state) {
            return state.email ?? ApplicationSettings.getString('email');
        },
        birthday (state) {
            return state.birthday ?? ApplicationSettings.getString('birthday');
        }
    }
};
