import express from 'express';
import axios from 'axios';

const router = express.Router();

/* Get all categories */
router.get('/', async (req, res) => {
    try {
        let categories = (await axios.get("/categories?display=full&output_format=JSON")).data.categories;
        res.json(categories);
    } catch (e) {
        res.status(500).json("Internal error");
    }
});

/* Get a category image */
router.get('/images/:categoryId', async (req, res) => {
    try {
        const { categoryId } = req.params;
        const data = (await axios.get(`/images/categories/${categoryId}`, { responseType: 'arraybuffer' })).data;
        return res.status(200).send(data);
    } catch (e) {
        console.error(e);
        return res.status(500).json('Internal error');
    }
});


export default router;
