import Vue from 'nativescript-vue';
import Vuex from "vuex";
import VueDevtools from 'nativescript-vue-devtools';
import axios from "axios";
import userStore from './store/user';
import productStore from './store/product';
import cartStore from './store/cart';
import categoryStore from './store/category';
import LoginPage from './pages/SigninPage';
import RadAutoComplete from 'nativescript-ui-autocomplete/vue';

axios.defaults.baseURL = `http://89.87.208.225:5666`;

if (TNS_ENV !== 'production') {
    Vue.use(VueDevtools);
}

Vue.use(Vuex);
Vue.use(RadAutoComplete);

const store = new Vuex.Store({
    modules: {
        products: productStore,
        user: userStore,
        cart: cartStore,
        category: categoryStore
    }
})

// Prints Vue logs when --env.production is *NOT* set while building
Vue.config.silent = (TNS_ENV === 'production');

new Vue({
    store,
    render: h => h('frame', [h(LoginPage)])
}).$start();
