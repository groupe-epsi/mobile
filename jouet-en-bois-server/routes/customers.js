import express from 'express';
import axios from 'axios';
import jstoxml from 'fast-xml-parser';

const Parser = jstoxml.j2xParser;
const router = express.Router();

// Update a customer
// For the moment, the id comes from the request body
// In the future, this call will only update the current logged customer (via session)
router.patch('/', async (req, res) => {
    const { firstname, lastname, email, birthday } = req.body;

    let schema = jstoxml.parse((await axios.get(`/customers?display=full&filter[email]=[${email}]`)).data);

    const customer = schema.prestashop.customers.customer;

    customer.firstname = firstname;
    customer.lastname = lastname;
    customer.email = email;
    customer.birthday = birthday;

    delete schema.prestashop.customers;
    schema.prestashop.customer = customer;

    const parser = new Parser({});
    const schemaXml = parser.parse(schema);

    try {
        const addedCustomer = await axios.put(`/customers?output_format=JSON`, schemaXml);
        let result = addedCustomer.data.customer;
        res.json(result);
    } catch (e) {
        console.debug(e.response.data);
    }
});

export default router;
