import * as express from 'express';
import axios from 'axios';

const router = express.Router();

/* Get all products */
router.get('/', async (req, res) => {
    try {
        const data = (await axios.get('/products?display=full&output_format=JSON')).data;
        const products = data.products.filter(product => product.state === "1");
        res.status(200).json(products);
    } catch (e) {
        console.error(e);
        return res.status(500).json('Internal error');
    }
});

/* Get a product image */
router.get('/images/:productId/:imageId/:quality', async (req, res) => {
    try {
        const { productId, imageId } = req.params;
        let quality = req.params.quality ?? "small_default";

        const data = (await axios.get(`/images/products/${productId}/${imageId}/${quality}`, { responseType: 'arraybuffer' })).data;
        return res.status(200).contentType("image/png").send(data);
    } catch (e) {
        console.error(e);
        return res.status(500).json('Internal error');
    }
});

export default router;
