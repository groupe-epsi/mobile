import * as express from 'express';
import axios from 'axios';
import bcrypt from 'bcryptjs';
import jstoxml from 'fast-xml-parser';

const Parser = jstoxml.j2xParser;
const router = express.Router();

/* Login with email and password */
router.post('/', async (req, res) => {
    const { email, password } = req.body;

    try {
        let url = `/customers?display=full&filter[email]=[${email}]&output_format=JSON`;
        const result = await axios.get(url);

        if (result.data.customers.length === 1) {
            const customerData = result.data.customers[0];
            const { firstname, lastname, email, birthday } = customerData;
            if (await bcrypt.compareSync(password, customerData.passwd)) {
                return res.status(200).json({
                    firstname,
                    lastname,
                    email,
                    birthday
                });
            } else {
                // Bad password
                return res.status(401).json('Bad credentials');
            }
        } else {
            // Bad email
            console.error(result.response.status);
            return res.status(401).json('Bad credentials');
        }
    } catch (error) {
        console.error(error.response);
        return res.status(500).json('Internal error');
    }
});

/* Create a customer */
router.post('/create', async (req, res) => {
    const { firstname, lastname, email, password, birthday } = req.body;

    let schema = jstoxml.parse((await axios.get(`/customers?schema=blank`)).data);

    const customer = schema.prestashop.customer;
    customer.firstname = firstname;
    customer.lastname = lastname;
    customer.email = email;
    customer.passwd = password;
    customer.birthday = birthday;

    const parser = new Parser({});
    const schemaXml = parser.parse(schema);

    try {
        const addedCustomer = await axios.post(`/customers?output_format=JSON`, schemaXml);
        let result = addedCustomer.data.customer;
        console.log(result);
        res.json(result);
    } catch (e) {
        console.error(e.response.data);
    }
});

export default router;
