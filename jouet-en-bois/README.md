# Compte rendu JEB (Jouet en bois)

## Sprint 1

### Installation

#### Environnement

Pour ce projet, je vais utiliser Gitlab comme contrôleur de version. J'ai utilisé l'outil gitlab "Boards" qui m'as
permis de créer des users-stories afin de découper le développement en plusieurs tâches.

#### Prestashop

Pour installer Prestashop, j'ai préféré utiliser des conteneurs avec Docker, j'ai donc utiliser une image MySQL, et une
image Prestashop.

    # Création d'un réseau pour que les deux conteneurs puissent communiquer
    $ docker network create prestashop-net
    # Lancement du conteneur MySQL
    $ docker run -ti --name some-mysql --network prestashop-net -e MYSQL_ROOT_PASSWORD=admin -p 3307:3306 -d mysql:5.7
    # Lancement du conteneur Prestashop
    $ docker run -ti --name some-prestashop --network prestashop-net -e DB_SERVER=some-mysql -p 8089:80 -d prestashop/prestashop

Ainsi, je peux accéder à ma base de donnée sur le port 3307, et à Prestashop sur le port 8089.

Pour pouvoir y accéder hors de mon réseau domestique, j'ai ajouté une règle de redirection NAT sur ma box.

![Alt text](assets/nat.png?raw=true "Regles NAT")

Je peux donc accéder à Prestashop depuis mon adresse IP public : http://89.87.208.225

J'ai aussi créé une règle NAT pour la future API tierce disponible avec le lien suivant : http://89.87.208.225:5666

## Sprint 2

Sur ce sprint, je me suis intéressé à l'API Prestashop, après avoir lu la documentation, j'ai créer une API pour gérer
la communication entre l'application et Prestashop.

### Prise en main

#### Prestashop

J'ai commencé à utiliser l'API Prestashop avec le logiciel Postman pour prendre en main, avec la documentation de l'API
prestashop : https://devdocs.prestashop.com/1.7/webservice/

Connexion à l'API en ajoutant l'authentification dans le header

![Alt text](assets/header_auth.png?raw=true "Header auth")

![Alt text](assets/products_result.png?raw=true "Product response example")

#### Création de l'API

J'ai ensuite réaliser une API tierce pour faciliter la communication appli/server, J'ai créée 4 routes au total :

- auth (pour la connexion)

- categories (pour les categories + images)

- customers (pour les informations d'un utilisateur)

- products (pour les produits + images)

Pour la route de connexion, Prestashop utilise la méthode de chiffrement bcrypt, ainsi, lors que l'API reçoit une
demande de connexion, l'API filtre les utilisateurs par mail, et compare ensuite les mots de passe chiffré grâce à la
librairie bcryptjs https://www.npmjs.com/package/bcryptjs

## Sprint 3

Pour ce sprint, je me suis occupé du front de mon projet, avec le framework Nativescript.

#### NativeScript

J'ai commencé l'installation de Nativescript sur mon ordinateur.

J'ai ensuite installé Android Studio ainsi qu'un émulateur Android pour pouvoir avoir visualisé mon projet.

L'option de la virtualisation AMD (AMD-V) n'étant pas activé par défaut, j'ai dû aller dans mon BIOS pour l'activer.

### Conception

J'ai commencé à mettre en oeuvre les différentes vues, en m'inspirant de l'application déjà existante de Prestashop.

J'ai utilisé la convention BEM pour mes classes css, qui, associé au framework scss rend les styles beaucoup plus
lisible

![img.png](src/assets/img.png)

## Sprint 4

Pour ce sprint, j'ai implementer la logique des composants et j'ai préparé la version définitive du produit.

### Implémentation de la logique

#### Le store

J'ai ajouté vuex (le store manager de Vue).

Pour les informations de l'utilisateur, j'ai utilisé le package application-settings qui permet de stocker des
informations dans le local storage de l'application, ainsi, quand l'utilisateur se connecte, il n'a plus besoin de se
reconnecter, a moins qu'il vide son local storage.

#### Communication avec le serveur

J'ai utilisé axios pour communiquer avec le serveur, je n'ai pas eu le temps d'utiliser les sessions, qui m'aurait
beaucoup aidé et régler pas mal de leger bug.

![img.png](src/assets/front_request.png)

#### Préparation de pour la démonstration

Je me suis connecté sur la version admin de Prestashop, j'y ai supprimé les anciens produits/categories, pour ajouter
des produits et catégories cohérents avec le sujet, par example, j'ai créé une catégorie "animaux", qui contient deux
sous catégorie "ferme" et "forêt", j'y ai ajouté des produits dans chaque sous-catégories

J'ai factorisé mon code, ajouté des commentaires pour expliquer le code difficile etc...

![img.png](src/assets/produits_prestashop.png)

## Conclusion

J'ai appris énormément de chose grâce à ce projet, que ce soit sur Nativescript, que j'utilisais pour la première fois,
ou la gestion du projet avec la gestion du temps, la réalisation d'un compte rendu, etc.

J'ai bien aimé le framework Nativescript, même si je préfère travailler en natif, ou avec des technologies Webview
(car j'ai beaucoup plus d'expérience dans l'intégration web).

Si je devais refaire le projet, je pense tout de même réutiliser Nativescript, car j'ai déjà des bases en VueJS, et que
je trouve le développement sur Nativescript beaucoup plus rapide que le natif sur le court terme.

Je pense pouvoir mettre à profit ce que j'ai appris dans le projet MSPR DNT2.
