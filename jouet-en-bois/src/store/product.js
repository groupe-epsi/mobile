import axios from 'axios';

export default {
    state: {
        products: [],
        selectedProduct: undefined
    },
    mutations: {
        setProducts (state, { products }) {
            state.products = products;
        },
        selectProduct (state, { product }) {
            state.selectedProduct = product;
        }
    },
    getters: {
        products (state) {
            return state.products;
        },
        getSelectedProduct (state) {
            return state.selectedProduct;
        },
    },
    actions: {
        async loadProducts (context) {
            try {
                const products = (await axios.get('/products')).data;
                context.commit('setProducts', { products });
            } catch (e) {
                console.error(e);
            }
        }
    }
};
