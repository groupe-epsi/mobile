import axios from 'axios';

export default {
    state: {
        categories: undefined,
        currentCategory: undefined,
    },
    mutations: {
        setCategories(state, { categories }) {
            state.categories = categories;
        },
        /**
         * Set the current selected category,
         *
         * @param state
         * @param category The category to select
         */
        setCurrentCategory(state, { category }) {
            state.currentCategory = category;
        },
    },
    getters: {
        categories(state) {
            return state.categories;
        },
        currentCategory(state) {
            return state.currentCategory;
        }
    },
    actions: {
        /**
         * Load categories from API
         */
        async loadCategories(context) {
            const categories = (await axios.get('/categories')).data;
            context.commit('setCategories', { categories });
        }
    }
};
