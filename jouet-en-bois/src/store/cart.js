export default {
    state: {
        cart: new Map(),
        finalPrice: 0,
        productCount: 0
    },
    mutations: {
        /**
         * Add a product to cart
         * This method does not update the final price
         * @param state
         * @param product The product to add to cart
         */
        addToCart (state, { product }) {
            const quantity = (state.cart.get(product) ?? 0) + 1;
            state.cart.set(product, quantity);
        },
        /**
         * Set a product's quantity
         * This method does not update the final price
         * @param state
         * @param product The product to update
         * @param quantity The product quantity to set in the cart
         */
        setQuantity (state, { product, quantity }) {
            state.cart.set(product, quantity);
        },
        /**
         * Remove a product from cart
         * This method does not update the final price
         * @param state
         * @param product The product to remove from cart
         */
        removeFromCart (state, { product }) {
            const quantity = state.cart.get(product) - 1;
            state.cart.set(product, quantity);
            if (quantity === 0) {
                state.cart.delete(product);
            }
        },
        /**
         * Update the final price
         * @param state
         */
        updateFinalPrice (state) {
            let finalPrice = 0;
            // Sum of all product's prices * quantity
            // Price of a product is in string so we need to parse it
            state.cart.forEach((quantity, product) => {
                finalPrice += Number.parseFloat(product.price) * quantity;
            });
            state.finalPrice = finalPrice;
        },
        /**
         * Update product count
         * @param state
         */
        updateProductCount (state) {
            let productCount = 0;
            state.cart.forEach(quantity => productCount += quantity);
            state.productCount = productCount;
        }
    },
    getters: {
        getCart (state) {
            return state.cart;
        },
        getFinalPrice (state) {
            return state.finalPrice;
        },
        cartProducts (state) {
            return state.cart;
        },
        getProductCount (state) {
            return state.productCount;
        }
    },
    actions: {
        /**
         * Add a product to cart, updates the final price and the product count
         * @param context
         * @param product The product to add to cart
         */
        addToCart (context, { product }) {
            context.commit('addToCart', { product });
            context.commit('updateFinalPrice');
            context.commit('updateProductCount');
        },
        /**
         * Remove a product from cart, update the final price and the product count
         * @param context
         * @param product The product to remove from cart
         */
        removeFromCart (context, { product }) {
            context.commit('removeFromCart', { product });
            context.commit('updateFinalPrice');
            context.commit('updateProductCount');
        },
        /**
         * Set a product's quantity, update the final price and the product count
         *
         * @param context
         * @param product The product to update
         * @param quantity The product quantity to set in the cart
         */
        setQuantity (context, { product, quantity }) {
            context.commit('setQuantity', { product, quantity });
            context.commit('updateFinalPrice');
            context.commit('updateProductCount');
        }
    }
};
